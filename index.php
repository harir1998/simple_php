<?php
$DB_HOSTNAME = 'mysql_db';
$DB_USERNAME = 'mysql_user';
$DB_PASSWORD = 'mysql_password';

try {
    $link = mysqli_connect($DB_HOSTNAME, $DB_USERNAME, $DB_PASSWORD);
    //If the exception is thrown, this text will not be shown
    if (!$link) {
        die('Could not connect');
    }
    echo 'Connected successfully!';
    echo 'Build 9';
    mysqli_close($link);
}
  
  //catch exception
catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
}



?>